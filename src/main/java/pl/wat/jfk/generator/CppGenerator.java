package pl.wat.jfk.generator;

import org.eclipse.jdt.core.dom.*;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 2015-06-13.
 */
public class CppGenerator {

    private int tabs;
    private CodeStringBuilder codeBuilder;
    private String lastAccessModifier = "";
    private static String classWithMain = null;
    private static String currentClass = null;

    public CppGenerator(CodeStringBuilder codeBuilder, int tabs) {
        this.codeBuilder = codeBuilder;
        this.tabs = tabs;
    }

    public int getTabs() {
        return tabs;
    }

    public CodeStringBuilder getCodeBuilder() {
        return codeBuilder;
    }

    private void line(Object... objects) {
        codeBuilder.line(tabs, objects);
    }

    private void inLine(Object... objects) {
        codeBuilder.inLine(tabs, objects);
    }

    private void firstInLine(Object... objects) {
        codeBuilder.firstInLine(tabs, objects);
    }

    private void newLine() {
        codeBuilder.newLine();
    }

    public void classBegin(TypeDeclaration typeDeclaration) {
        firstInLine("class ", typeDeclaration.getName());
        currentClass = typeDeclaration.getName().toString();
        List<String> superClasses = new ArrayList<>();
        if(typeDeclaration.getSuperclassType() != null) {
            superClasses.add(typeDeclaration.getSuperclassType().toString());
        }
        for(Object object : typeDeclaration.superInterfaceTypes()) {
            superClasses.add(object.toString());
        }
        if(!superClasses.isEmpty()) {
            inLine(": ");
            for(int i = 0; i < superClasses.size() - 1; i++) {
                inLine("public ",superClasses.get(i), ", ");
            }
            inLine("public ",superClasses.get(superClasses.size() - 1));
        }
        inLine(" {");
        newLine();
        tabs++;
    }

    public void classEnd() {
        tabs--;
        line("};");
    }

    public void methodBegin(MethodDeclaration methodDeclaration) {
        checkIsMain(methodDeclaration);
        int modifiers = methodDeclaration.getModifiers();
        fieldAccess(modifiers);
        firstInLine(modifiers(modifiers), " ");
        if(methodDeclaration.getReturnType2()!=null) {
            String virtual = "virtual ";
            if(Modifier.isStatic(modifiers)) {
                virtual = "";
            }
            if(methodDeclaration.getReturnType2().isArrayType()) {
                inLine(virtual, arrayTypeToString((ArrayType) methodDeclaration.getReturnType2()), methodDeclaration.getName(), "(");
            } else {
                inLine(virtual, typeName(methodDeclaration.getReturnType2().toString()), " ", methodDeclaration.getName(), "(");
            }
        } else {
            inLine(methodDeclaration.getName() + "(");
        }
        if(!methodDeclaration.parameters().isEmpty()) {
            methodParameters(methodDeclaration.parameters());
        }
        inLine(") {");
        newLine();
        tabs++;
    }

    private void checkIsMain(MethodDeclaration methodDeclaration) {
        if(methodDeclaration.getName().toString().equals("main") && (Modifier.isStatic(methodDeclaration.getModifiers()))) {
            classWithMain = currentClass;
        }
    }

    private void methodParameters(List<SingleVariableDeclaration> parameters) {
        for(int i = 0; i < parameters.size()-1; i++) {
            SingleVariableDeclaration singleVariableDeclaration = parameters.get(i);
            inLine(methodParameterToString(singleVariableDeclaration), ", ");
        }
        SingleVariableDeclaration singleVariableDeclaration = parameters.get(parameters.size() - 1);
        inLine(methodParameterToString(singleVariableDeclaration));
    }

    private String methodParameterToString(SingleVariableDeclaration singleVariableDeclaration) {
        if(singleVariableDeclaration.getType().isArrayType()) {
            return arrayTypeToString((ArrayType)singleVariableDeclaration.getType()) + singleVariableDeclaration.getName().toString();
        } else {
            return typeName(singleVariableDeclaration.getType().toString()) + " " + singleVariableDeclaration.getName();
        }
    }

    public void forBegin(ForStatement statement) {
        firstInLine("for(");
        forInitializers(statement);
        inLine("; ");
        expression(statement.getExpression());
        inLine("; ");
        forUpdaters(statement);
        inLine(") {");
        newLine();
        tabs++;
    }

    private void forInitializers(ForStatement statement) {
        List<Expression> initializers = statement.initializers();
        if(!initializers.isEmpty()) {
            for (int i = 0; i < initializers.size() - 1; i++) {
                expression(initializers.get(i));
                inLine(", ");
            }
            expression(initializers.get(initializers.size() - 1));
        }
    }

    private void forUpdaters(ForStatement statement) {
        List<Expression> updaters = statement.updaters();
        if(!updaters.isEmpty()) {
            for (int i = 0; i < updaters.size() - 1; i++) {
                expression(updaters.get(i));
                inLine(", ");
            }
            expression(updaters.get(updaters.size() - 1));
        }
    }

    public void foreachBegin(EnhancedForStatement foreachStatement) {
        line("int ", foreachStatement.getExpression(), "Length = sizeof(", foreachStatement.getExpression(),") / sizeof(", foreachStatement.getExpression(), "[0]);");
        line("for(int iterator = 0; iterator < ", foreachStatement.getExpression(),"Length; iterator++) {");
        tabs++;
        line(typeName(foreachStatement.getParameter().getType().toString()), " ", foreachStatement.getParameter().getName(), " = ",
                foreachStatement.getExpression().toString(), "[iterator];");
    }

    public void ifBegin(IfStatement ifStatement) {
        firstInLine("if(");
        expression(ifStatement.getExpression());
        inLine(") {");
        newLine();
        tabs++;
    }

    public void elseBegin(IfStatement ifStatement) {
        firstInLine("else {");
        newLine();
        tabs++;
    }

    public void doBegin(DoStatement doStatement) {
        line("do {");
        tabs++;
    }

    public void doEnd(DoStatement doStatement) {
        tabs--;
        firstInLine("} while (");
        expression(doStatement.getExpression());
        inLine(");");
        newLine();
    }

    public void whileBegin(WhileStatement whileStatement) {
        firstInLine("while (");
        expression(whileStatement.getExpression());
        inLine(") {");
        newLine();
        tabs++;
    }

    public void blockEnd() {
        tabs--;
        line("}");
    }

    public void fieldDeclaration(FieldDeclaration node) {
        int modifiers = node.getModifiers();
        fieldAccess(modifiers);
        if(node.getModifiers() != 0) {
            firstInLine(modifiers(node.getModifiers()), " ");
        }
        if(node.getType().isArrayType()) {
            ArrayType arrayType = (ArrayType)node.getType();
            inLine(arrayTypeToString(arrayType));
            VariableDeclarationFragment variableDeclarationFragment = (VariableDeclarationFragment)node.fragments().get(0);
            inLine(variableDeclarationFragment);
            inLine(";");
            newLine();
        } else {
            inLine(typeName(node.getType().toString()), " ");
            Object[] fragments = node.fragments().toArray();
            for(int i=0; i<fragments.length - 1; i++) {
                VariableDeclarationFragment fragment = (VariableDeclarationFragment) fragments[i];
                variableDeclarationFragment(fragment);
                inLine(", ");
            }
            VariableDeclarationFragment fragment = (VariableDeclarationFragment) fragments[fragments.length - 1];
            variableDeclarationFragment(fragment);
            inLine(";");
            newLine();
        }
    }

    private String arrayTypeToString(ArrayType arrayType) {
        Type elementType = arrayType.getElementType();
        return typeName(elementType.toString()) + " *";
    }

    private void variableDeclarationFragment(VariableDeclarationFragment fragment) {
        inLine(typeName(fragment.getName().toString()), " ");
        Expression initializeExpression = fragment.getInitializer();
        if(initializeExpression != null) {
            if(initializeExpression instanceof ClassInstanceCreation) {
                ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation)initializeExpression;
                if(!classInstanceCreation.arguments().isEmpty()) {
                    inLine(" = ");
                }
            } else {
                inLine(" = ");
            }
            expression(initializeExpression);
        }
    }

    private void expression(Expression expression) {
        if(expression instanceof ArrayAccess) {
            ArrayAccess arrayAccess = (ArrayAccess) expression;
            expression(arrayAccess.getArray());
            inLine("[");
            expression(arrayAccess.getIndex());
            inLine(("]"));
            return;
        }
        if(expression instanceof  Assignment) {
            Assignment assignment = (Assignment)expression;
            expression(assignment.getLeftHandSide());
            inLine(" ", assignment.getOperator(), " ");
            expression(assignment.getRightHandSide());
            return;
        }
        if(expression instanceof  CastExpression) {
            CastExpression castExpression = (CastExpression) expression;
            inLine("(",typeName(castExpression.getType().toString()),")");
            expression(castExpression.getExpression());
            return;
        }
        if(expression instanceof ClassInstanceCreation) {
            ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation)expression;
            List<Expression> arguments = classInstanceCreation.arguments();
            if(!arguments.isEmpty()) {
                inLine(classInstanceCreation.getType());
                inLine("(");
                for(int i = 0; i < arguments.size() - 1; i++) {
                    expression(arguments.get(i));
                    inLine(", ");
                }
                expression(arguments.get(arguments.size()-1));
                inLine(")");
            }
            return;
        }
        if(expression instanceof ConditionalExpression) {
            ConditionalExpression conditionalExpression = (ConditionalExpression) expression;
            expression(conditionalExpression.getExpression());
            inLine(" ? ");
            expression(conditionalExpression.getThenExpression());
            inLine(" : ");
            expression(conditionalExpression.getElseExpression());
            return;
        }
        if(expression instanceof  FieldAccess) {
            FieldAccess fieldAccess = (FieldAccess)expression;
            expression(fieldAccess.getExpression());
            inLine(".",fieldAccess.getName());
            return;
        }
        if(expression instanceof InfixExpression) {
            InfixExpression infixExpression = (InfixExpression)expression;
            expression(infixExpression.getLeftOperand());
            inLine(" ", infixExpression.getOperator(), " ");
            expression(infixExpression.getRightOperand());
            return;
        }
        if(expression instanceof InstanceofExpression) {
            throw new RuntimeException("instanceof nie jest wspierane");
        }
        if(expression instanceof MethodInvocation) {
            MethodInvocation methodInvocation = (MethodInvocation)expression;
            if(methodInvocation.getExpression()!=null) {
                expression(methodInvocation.getExpression());
                inLine(".");
            }
            inLine(methodInvocation.getName());
            inLine("(");
            List<Expression> arguments = methodInvocation.arguments();
            if(!arguments.isEmpty()) {
                for(int i = 0; i < arguments.size() - 1; i++) {
                    expression(arguments.get(i));
                    inLine(", ");
                }
                expression(arguments.get(arguments.size()-1));
            }
            inLine(")");
            return;
        }
        if(expression instanceof NullLiteral) {
            inLine("NULL");
            return;
        }
        if(expression instanceof ParenthesizedExpression) {
            ParenthesizedExpression parenthesizedExpression = (ParenthesizedExpression)expression;
            inLine("(");
            expression(parenthesizedExpression.getExpression());
            inLine(")");
            return;
        }
        if(expression instanceof PostfixExpression) {
            PostfixExpression postfixExpression = (PostfixExpression)expression;
            expression(postfixExpression.getOperand());
            inLine(postfixExpression.getOperator());
            return;
        }
        if(expression instanceof PrefixExpression) {
            PrefixExpression prefixExpression = (PrefixExpression)expression;
            inLine(prefixExpression.getOperator());
            expression(prefixExpression.getOperand());
            return;
        }
        if(expression instanceof SuperFieldAccess) {
            /*SuperFieldAccess superFieldAccess = (SuperFieldAccess)expression;
            inLine("base.");
            expression(superFieldAccess.getName());
            return;*/
            throw new RuntimeException("super nie jest wspierane");
        }
        if(expression instanceof SuperMethodInvocation) {
            /*SuperMethodInvocation superMethodInvocation = (SuperMethodInvocation)expression;
            inLine("base.");
            inLine(superMethodInvocation.getName());
            inLine("(");
            List<Expression> arguments = superMethodInvocation.arguments();
            if(!arguments.isEmpty()) {
                for(int i = 0; i < arguments.size() - 1; i++) {
                    expression(arguments.get(i));
                    inLine(", ");
                }
                expression(arguments.get(arguments.size()-1));
            }
            inLine(")");
            return;*/
            throw new RuntimeException("super nie jest wspierane");
        }
        if(expression instanceof VariableDeclarationExpression) {
            VariableDeclarationExpression variableDeclarationExpression = (VariableDeclarationExpression)expression;
            if(variableDeclarationExpression.getModifiers() != 0) {
                inLine(modifiers(variableDeclarationExpression.getModifiers()), " ");
            }
            if(variableDeclarationExpression.getType().isArrayType()) {
                inLine(arrayTypeToString((ArrayType) variableDeclarationExpression.getType()));
            } else {
                inLine(variableDeclarationExpression.getType(), " ");
            }
            List<VariableDeclarationFragment> fragments = variableDeclarationExpression.fragments();
            if(!fragments.isEmpty()) {
                for(int i =0; i < fragments.size() - 1; i++) {
                    variableDeclarationFragment(fragments.get(i));
                    inLine(", ");
                }
                variableDeclarationFragment(fragments.get(fragments.size()-1));
            }
            return;
        }
        if(expression instanceof Name) {
            Name name = (Name) expression;
            if(name.isSimpleName()) {
                SimpleName simpleName = (SimpleName)name;
                inLine(typeName(simpleName.toString()));
                return;
            }
        }
        if(expression instanceof ThisExpression) {
            inLine(("(*this)"));
            return;
        }

        inLine(expression);
    }

    public void expressionStatement(ExpressionStatement node) {
        firstInLine();
        expression(node.getExpression());
        inLine(";");
        newLine();
    }

    public void variableDeclarationStatement(VariableDeclarationStatement node) {
        firstInLine();
        if(node.getModifiers() != 0) {
            inLine(modifiers(node.getModifiers()), " ");
        }
        if(node.getType().isArrayType()) {
            inLine(arrayTypeToString((ArrayType)node.getType()));
        } else {
            inLine(typeName(node.getType().toString()), " ");
        }
        List<VariableDeclarationFragment> fragments = node.fragments();
        if(!fragments.isEmpty()) {
            for(int i =0; i < fragments.size() - 1; i++) {
                variableDeclarationFragment(fragments.get(i));
                inLine(", ");
            }
            variableDeclarationFragment(fragments.get(fragments.size()-1));
        }
        inLine(";");
        newLine();
    }

    private void fieldAccess(int modifiers) {
        String accessModifier = access(modifiers);
        if(!accessModifier.equals(lastAccessModifier)) {
            tabs--;
            line(accessModifier, ": ");
            tabs++;
            lastAccessModifier = accessModifier;
        }
    }

    private String access(int modifiers) {
        if(Modifier.isPublic(modifiers)) {
            return "  public";
        }
        if(Modifier.isPrivate(modifiers)) {
            return "  private";
        }
        if(Modifier.isProtected(modifiers)) {
            return "  protected";
        }
        return "  public";
    }

    private String modifiers(int modifiers) {
        String string = "";
        if(Modifier.isFinal(modifiers)) {
            string += " const";
        }
        if(Modifier.isStatic(modifiers)) {
            string += " static";
        }
        if(Modifier.isAbstract(modifiers)) {
            string += " virtual";
        }
        return string.trim();
    }

    private String typeName(String string) {
        if(string.equals("boolean")) {
            return "bool";
        }
        if(string.equals("String")) {
            return "string";
        }
        return string;
    }

    public void emptyStatement() {
        line(";");
    }

    public void returnStatement(ReturnStatement statement) {
        firstInLine("return ");
        expression(statement.getExpression());
        inLine(";");
        newLine();
    }

    public void fileBegin() {
        line("#include<iostream>");
        line("using namespace std;");
        newLine();
    }

    public void fileEnd() {
        if(classWithMain != null) {
            line("int main() {");
            line("\t", classWithMain, "::main(NULL);");
            line("\treturn 0;");
            line("}");
        }
    }

}
