package pl.wat.jfk.generator;

/**
 * Created by Mateusz on 2015-06-13.
 */
public class CodeStringBuilder {

    public static final String NEW_LINE = "\r\n";

    private StringBuilder generatedCode = new StringBuilder();

    public String getGeneratedCode() {
        return generatedCode.toString();
    }

    private StringBuilder stringBuilder(int tabs, boolean addTabs, Object... objects) {
        StringBuilder stringBuilder = new StringBuilder();
        if (addTabs) {
            for (int i = 0; i < tabs; i++) {
                stringBuilder.append("\t");
            }
        }
        for (Object object : objects) {
            stringBuilder.append(object);
        }
        return stringBuilder;
    }

    public void line(int tabs, Object... objects) {
        StringBuilder stringBuilder = stringBuilder(tabs, true, objects);
        stringBuilder.append(NEW_LINE);
        generatedCode.append(stringBuilder.toString());
    }

    public void inLine(int tabs, Object... objects) {
        StringBuilder stringBuilder = stringBuilder(tabs, false, objects);
        generatedCode.append(stringBuilder.toString());
    }

    public void firstInLine(int tabs, Object... objects) {
        StringBuilder stringBuilder = stringBuilder(tabs, true, objects);
        generatedCode.append(stringBuilder.toString());
    }

    public void newLine() {
        generatedCode.append(NEW_LINE);
    }

}
