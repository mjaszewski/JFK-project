package pl.wat.jfk.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;

/**
 * Created by Mateusz on 2015-06-13.
 */
public class ClassVisitor extends ASTVisitor{

    private CppGenerator cppGenerator;
    private CodeStringBuilder codeStringBuilder;
    private boolean innerClassVisitor;

    public ClassVisitor(CppGenerator cppGenerator) {
        this.cppGenerator = cppGenerator;
        codeStringBuilder = cppGenerator.getCodeBuilder();
        innerClassVisitor = false;
    }

    public ClassVisitor(CppGenerator cppGenerator, boolean innerClassVisitor) {
        this(cppGenerator);
        this.innerClassVisitor = innerClassVisitor;
    }

    @Override
    public boolean visit(TypeDeclaration node) {
        if (node.isInterface()) {
            return false;
        }
        if(node.isPackageMemberTypeDeclaration() == innerClassVisitor) {
            return false;
        }

        cppGenerator.classBegin(node);

        CppGenerator newCppGenerator = new CppGenerator(codeStringBuilder, cppGenerator.getTabs());

        for(TypeDeclaration typeDclarationNode : node.getTypes()) {
            typeDclarationNode.accept(new ClassVisitor(newCppGenerator, true));
        }
        node.accept(new FieldVisitor(newCppGenerator));
        node.accept(new MethodDeclarationVisitor(newCppGenerator));

        cppGenerator.classEnd();

        return false;
    }
}
