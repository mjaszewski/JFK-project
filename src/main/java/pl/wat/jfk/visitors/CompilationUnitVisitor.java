package pl.wat.jfk.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;

/**
 * Created by Mateusz on 2015-06-13.
 */
public class CompilationUnitVisitor extends ASTVisitor {

    private CppGenerator cppGenerator;
    private CodeStringBuilder codeStringBuilder;

    public CompilationUnitVisitor(CppGenerator cppGenerator) {
        this.cppGenerator = cppGenerator;
        this.codeStringBuilder = cppGenerator.getCodeBuilder();
    }

    @Override
    public boolean visit(CompilationUnit node) {
        CppGenerator newCppGenerator = new CppGenerator(codeStringBuilder, cppGenerator.getTabs());
        cppGenerator.fileBegin();
        node.accept(new ClassVisitor(newCppGenerator));
        cppGenerator.fileEnd();
        return false;
    }
}
