package pl.wat.jfk.visitors;

import org.eclipse.jdt.core.dom.*;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;

import java.util.List;

/**
 * Created by Mateusz on 2015-06-14.
 */
public class BlockVisitor extends ASTVisitor {
    private CppGenerator cppGenerator;
    private CodeStringBuilder codeStringBuilder;

    public BlockVisitor(CppGenerator cppGenerator) {
        this.cppGenerator = cppGenerator;
        codeStringBuilder = cppGenerator.getCodeBuilder();
    }

    public boolean visit(Block node) {
        visitBlockOrStatement(node);
        return false;
    }

    private void visitBlockOrStatement(Statement node) {
        if(node instanceof Block) {
            Block block = (Block)node;
            List<Statement> statmentList = block.statements();
            for(Statement statement : statmentList) {
                visitStatement(statement);
            }
        } else {
            visitStatement(node);
        }
    }

    private void visitStatement(Statement statement) {
        if(statement instanceof ExpressionStatement) {
            cppGenerator.expressionStatement((ExpressionStatement)statement);
        }
        if(statement instanceof VariableDeclarationStatement) {
            cppGenerator.variableDeclarationStatement((VariableDeclarationStatement) statement);
        }
        if(statement instanceof ForStatement) {
            ForStatement forStatement = (ForStatement) statement;
            cppGenerator.forBegin(forStatement);
            visitBlockOrStatement(forStatement.getBody());
            cppGenerator.blockEnd();
        }
        if(statement instanceof EnhancedForStatement) {
            EnhancedForStatement foreachStatement = (EnhancedForStatement)statement;
            cppGenerator.foreachBegin(foreachStatement);
            visitBlockOrStatement(foreachStatement.getBody());
            cppGenerator.blockEnd();
        }
        if(statement instanceof IfStatement) {
            IfStatement ifStatement = (IfStatement)statement;
            cppGenerator.ifBegin(ifStatement);
            visitBlockOrStatement(ifStatement.getThenStatement());
            cppGenerator.blockEnd();
            if(ifStatement.getElseStatement() != null) {
                cppGenerator.elseBegin(ifStatement);
                visitBlockOrStatement(ifStatement.getElseStatement());
                cppGenerator.blockEnd();
            }
        }
        if(statement instanceof DoStatement) {
            DoStatement doStatement = (DoStatement)statement;
            cppGenerator.doBegin(doStatement);
            visitBlockOrStatement(doStatement.getBody());
            cppGenerator.doEnd(doStatement);
        }
        if(statement instanceof WhileStatement) {
            WhileStatement whileStatement = (WhileStatement)statement;
            cppGenerator.whileBegin(whileStatement);
            visitBlockOrStatement(whileStatement.getBody());
            cppGenerator.blockEnd();
        }
        if(statement instanceof EmptyStatement) {
            cppGenerator.emptyStatement();
        }
        if(statement instanceof ReturnStatement) {
            cppGenerator.returnStatement((ReturnStatement)statement);
        }
    }

}
