package pl.wat.jfk.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;

/**
 * Created by Mateusz on 2015-06-14.
 */
public class FieldVisitor extends ASTVisitor {

    private CppGenerator cppGenerator;
    private CodeStringBuilder codeStringBuilder;

    public FieldVisitor(CppGenerator cppGenerator) {
        this.cppGenerator = cppGenerator;
        codeStringBuilder = cppGenerator.getCodeBuilder();
    }

    @Override
    public boolean visit(FieldDeclaration node) {
        cppGenerator.fieldDeclaration(node);
        return false;
    }
}
