package pl.wat.jfk.visitors;

import org.eclipse.jdt.core.dom.*;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;

/**
 * Created by Mateusz on 2015-06-14.
 */
public class MethodDeclarationVisitor extends ASTVisitor {

    private CppGenerator cppGenerator;
    private CodeStringBuilder codeStringBuilder;

    public MethodDeclarationVisitor(CppGenerator cppGenerator) {
        this.cppGenerator = cppGenerator;
        codeStringBuilder = cppGenerator.getCodeBuilder();
    }

    @Override
    public boolean visit(MethodDeclaration node) {
        cppGenerator.methodBegin(node);
        node.getBody().accept(new BlockVisitor(new CppGenerator(codeStringBuilder, cppGenerator.getTabs())));
        cppGenerator.blockEnd();
        return false;
    }
}
