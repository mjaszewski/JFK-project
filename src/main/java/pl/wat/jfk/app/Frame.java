package pl.wat.jfk.app;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.Scanner;

/**
 * Created by Mateusz on 2015-06-15.
 */
public class Frame extends JFrame {

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem saveMenuItem, openMenuItem, translateMenuItem;

    private JScrollPane sourceScrollPane;
    private JScrollPane destinationScrollPane;

    private JTextArea sourceTextArea;
    private JTextArea destinationTextArea;

    private JPanel sourcePanel;
    private JPanel destinationPanel;

    public Frame() {
        super("Java 2 C++");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800,600);
        setLayout(new GridLayout(1, 2));
        sourcePanel = new JPanel();
        sourcePanel.setLayout(new BorderLayout());
        sourcePanel.setBorder(BorderFactory.createTitledBorder("Kod źródłowy - Java"));
        sourceTextArea = new JTextArea();
        sourceTextArea.setTabSize(4);
        sourceScrollPane = new JScrollPane(sourceTextArea);
        sourcePanel.add(sourceScrollPane, BorderLayout.CENTER);

        destinationTextArea = new JTextArea();
        destinationTextArea.setEditable(false);
        destinationTextArea.setTabSize(4);
        destinationPanel = new JPanel();
        destinationPanel.setLayout(new BorderLayout());
        destinationPanel.setBorder(BorderFactory.createTitledBorder("Kod wynikowy - C++"));
        destinationScrollPane = new JScrollPane(destinationTextArea);
        destinationPanel.add(destinationScrollPane, BorderLayout.CENTER);

        add(sourcePanel);
        add(destinationPanel);

        createMenu();
        setJMenuBar(menuBar);

        setVisible(true);
    }

    private void createMenu() {
        menuBar = new JMenuBar();
        fileMenu = new JMenu("Plik");
        saveMenuItem = new JMenuItem("Zapisz");
        openMenuItem = new JMenuItem("Otwórz");
        translateMenuItem = new JMenuItem("Tłumacz");

        saveMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });

        openMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                open();
            }
        });

        translateMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                translate();
            }
        });

        fileMenu.add(openMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(translateMenuItem);

        menuBar.add(fileMenu);
    }

    private void open() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Java", "java");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = chooser.getSelectedFile();
                Scanner scanner = new Scanner(new FileReader(file));
                scanner.useDelimiter("\\A");
                String source = scanner.next();
                sourceTextArea.setText(source);
                scanner.close();
                translate();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void save() {
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = chooser.getSelectedFile();
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(destinationTextArea.getText());
                fileWriter.flush();
                fileWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void translate() {
        Java2CppTranslator java2CppTranslator = new Java2CppTranslator();
        String code = java2CppTranslator.translate(sourceTextArea.getText());
        destinationTextArea.setText(code);
    }

}
