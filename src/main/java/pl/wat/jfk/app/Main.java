package pl.wat.jfk.app;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static String PATH = "C:\\Mateusz\\programowanie\\jfk-projekt\\src\\main\\java\\pl\\wat\\jfk\\test\\Test3.java";

    private Java2CppTranslator java2CppTranslator = new Java2CppTranslator();

    public void run() {

        File file = new File(PATH);

        try {

            Scanner scanner = new Scanner(new FileReader(file));
            scanner.useDelimiter("\\A");
            String source = scanner.next();

            System.out.printf(java2CppTranslator.translate(source));

        } catch (FileNotFoundException e) {
        }
    }

    public static void main(String args[]) {
        //new Main().run();
        new Frame();
    }
}
