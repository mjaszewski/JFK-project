package pl.wat.jfk.app;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import pl.wat.jfk.generator.CodeStringBuilder;
import pl.wat.jfk.generator.CppGenerator;
import pl.wat.jfk.visitors.CompilationUnitVisitor;

/**
 * Created by Mateusz on 2015-06-13.
 */
public class Java2CppTranslator {

    private CodeStringBuilder codeStringBuilder = new CodeStringBuilder();
    private CompilationUnitVisitor compilationUnitVisitor = new CompilationUnitVisitor(new CppGenerator(codeStringBuilder, 0));

    public String translate(String source) {
        ASTParser parser = ASTParser.newParser(AST.JLS3);
        parser.setSource(source.toCharArray());
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

        compilationUnitVisitor.visit(cu);

        return codeStringBuilder.getGeneratedCode();
    }

}
